# from geopy.geocoders import Nominatim
# geolocator = Nominatim()
import sqlite3
from crfn import db_path

con = sqlite3.connect(db_path)

cur = con.cursor()






## search the range, just do a square
def searchdb(lat, long, km):
    import sqlite3

    con = sqlite3.connect(db_path)

    cur = con.cursor()

    if km < 0 or km > 15:
        return []

    #http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    rate = 111111/1 #m per degree

    degree = (km*1000/111111)
    latd = lat - degree
    latu = lat + degree
    longl = long - degree
    longr = long + degree

    stringdb = '''SELECT * FROM bqp_data WHERE LATITUDE BETWEEN {0} AND {1}
            AND LONGITUDE BETWEEN {2} AND {3}'''.format(latu, latd, longl, longr)

    # stringdb = '''SELECT * FROM bqp_data WHERE LATITUDE BETWEEN {0} AND {1}'''.format(latu, latd)

    cur.execute(stringdb)

    result = cur.fetchall()

    data = []
    for i in result:
        fix = list(i)
        fix[20] = float(fix[20])
        fix[21] = float(fix[21])
        fix[17] = float(fix[17])
        data.append(fix)

    con.close()


    return data

# print(searchdb(-27.466581, 153.023369, 10))