import os
import pandas
import sqlite3

con = sqlite3.connect('crfn.db')

cur = con.cursor()

# print (os.getcwd())

# os.chdir("./RawData")

# print (os.getcwd())

# file = open("BQP_DATA_v4.csv", "r")


# while True:
#     line = file.readline()
#     print(line)
#     a = input()

# file.close()

a = pandas.read_csv("./BQP_DATA_v4.csv")

b = a.get_values()

for i, j in enumerate(b):
    print(i)

    array = b[i]

    c = '''INSERT INTO bqp_data (DA_NUMBER,
    ESA_NAME,
    ESA_STATE,
    PREMISES_COUNT,
    AVAILABILITY,
    AVAILABILITY_FTTP_RATING,
    AVAILABILITY_HFC_RATING,
    AVAILABILITY_FTTN_RATING,
    AVAILABILITY_WIRELESS_RATING,
    AVAILABILITY_ADSL_RATING,
    QUALITY,
    QUALITY_FTTP_RATING,
    QUALITY_HFC_RATING,
    QUALITY_FTTN_RATING,
    QUALITY_NBNW_RATING,
    QUALITY_ADSL_RATING,
    ADSL_SPEED_MEDIAN,
    AVAILABILITY_MOBILE_RATING,
    QUALITY_MOBILE_RATING)
    VALUES ("{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}","{8}","{9}","{10}","{11}","{12}","{13}","{14}","{15}","{16}","{17}","{18}")'''.format(
    array[0],
    array[1],
    array[2],
    array[3],
    array[4],
        array[5],
        array[6],
        array[7],
        array[8],
        array[9],
        array[10],
        array[11],
        array[12],
        array[13],
        array[14],
        array[15],
        array[16],
        array[17],
        array[18],
    )

    cur.execute(c)


con.commit()

con.close()