import sqlite3


con = sqlite3.connect('crfn.db')

cur = con.cursor()


databasestr = "SELECT * FROM bqp_data"


cur.execute(databasestr)


rows = cur.fetchall()

for i, row in enumerate(rows):
    # print(row)
    id = row[0]
    esaname = row[1]
    # print (id, esaname)

    whatislatlong = '''SELECT * FROM da_centroids WHERE DA_NUMBER= "{0}"'''.format(esaname)
    # print(whatislatlong)

    cur.execute(whatislatlong)
    latlongquery = cur.fetchone()
    # print(latlongquery)

    long = latlongquery[3]
    lat = latlongquery[4]

    updating = '''UPDATE bqp_data SET LATITUDE = "{0}", LONGITUDE = "{1}" WHERE id = {2}'''.format(lat, long, id)
    # print(updating)
    cur.execute(updating)

    # input()
    con.commit()
    print(i)



con.commit()

con.close()