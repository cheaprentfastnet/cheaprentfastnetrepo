import sqlite3

con = sqlite3.connect('crfn.db')
cur = con.cursor()

# Get median rent for victorian postcodes
def get_vic_median(num_rooms, dwelling_type, postcode):
    c = '''SELECT * FROM lga_to_postcode WHERE POSTCODE = {}'''
    cur.execute(c.format(postcode))
    x = cur.fetchone()
    if x is None:
        return None
    lga = x[2]

    # TODO: Consider combining. I forget why I decided this was efficient.
    c = '''
        SELECT MEDIAN_RENT FROM vic_rent WHERE LOCALITY = '{}' AND BEDROOMS = {} AND DWELLING_TYPE = '{}'
        '''.format(lga, num_rooms, dwelling_type)
    cur.execute(c)
    x = cur.fetchone()
    if x is None:
        print("2nd query fail")
        return None
    median_rent = x

    return median_rent[0]

if __name__ == "__main__":
    print(get_vic_median(3, "Flat", 3000))
