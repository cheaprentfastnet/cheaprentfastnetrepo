from flask import Flask
from flask import render_template
from flask import request, abort
import Perimeter
import os.path

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "crfn.db")

import googlemaps

# try:
#     from .forms import AcceptStreet
# except Exception: #ImportError
#     from forms import AcceptStreet

app = Flask(__name__)
try:
    app.config.from_object("config")
except Exception:
    print("You must create a file with the correct API keys. Ask Dylan")

@app.route("/")
def main():
    return render_template("midblock.html")

@app.route("/findcheap", methods=['GET', 'POST'])
def findcheap():


    if request.method == 'POST':
        # check Radius

        try:
            radius = int(request.form['searchradius'])

            if radius < 0:
                raise Exception
            if radius > 15:
                radius = 15
        except:
            return render_template("errorpage.html")

        mapaddress = request.form['streetaddress']
        gmaps = googlemaps.Client(key=app.config["MAPS_KEY"])
        location = gmaps.geocode(mapaddress, region="AU")

        if location is None or len(location) < 1:
            return abort(404)

        coords = location[0]["geometry"]["location"]
        # print(coords)

        locationdata = {
            'location': location[0]["formatted_address"],
            'lat': coords["lat"],
            'long': coords["lng"]
        }

        datapoints = Perimeter.searchdb(locationdata.get('lat'), locationdata.get('long'), radius)

        # return render_template("map.html", locationdata=locationdata, datapoints=datapoints)

        return render_template("map.html", locationdata=locationdata, datapoints=datapoints)

    return render_template("midblock.html")


@app.route("/about")
def about():
    return render_template("about.html")

if __name__ == "__main__":
    app.run(host='0.0.0.0')